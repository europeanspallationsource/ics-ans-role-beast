import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_alarm_server_services(host):
    service = host.service('alarm-server@MAIN')
    assert service.is_running
    assert service.is_enabled


def test_alarm_config_list(host):
    cmd = host.command('cd /opt/alarm-config; ./AlarmConfigTool -list')
    assert 'MAIN' in cmd.stdout
