---
- name: clone the beast-config repository
  git:
    repo: "{{ beast_config_repo }}"
    dest: /opt/alarm-config/beast-config
    version: "{{ beast_config_repo_version }}"

- name: look for specific alarm configuration files
  find:
    paths: "/opt/alarm-config/beast-config/{{ ansible_fqdn }}"
    patterns: '*.xml'
  register: specific_beast_config

- name: look for default alarm configuration files
  find:
    paths: /opt/alarm-config/beast-config/default
    patterns: '*.xml'
  register: default_beast_config
  when: not specific_beast_config.files

# It would be nice to use the same variable in previous tasks
# to avoid having to combine them,
# but the register variable is always overwritten when there is a conditional...
# See https://github.com/ansible/ansible/issues/4297
# Note that using block as mentioned in one comment doesn't work (Ansible 2.4.2)
- name: set beast config files to use
  set_fact:
    beast_config_files: "{{ specific_beast_config.files if specific_beast_config.files else default_beast_config.files }}"

- name: display beast config files
  debug:
    msg: "{{ beast_config_files | map(attribute='path') | join(', ') }}"

- name: set beast root components
  set_fact:
    beast_root_components: "{{ beast_config_files | map(attribute='path') | map('basename') | map('replace', '.xml', '') | list }}"

# Copy files locally to trigger the handlers on change
# We can't easily do that on the git clone task because the
# git repository contains configuration for different servers
- name: copy alarm config files locally to trigger the update
  copy:
    src: "{{ item.path }}"
    dest: "/opt/alarm-config/{{ item.path | basename }}"
    remote_src: true
    owner: alarm
    group: alarm
    mode: 0644
  with_items: "{{ beast_config_files }}"
  register: copy_config
  notify:
    - update the alarm configuration(s)
    - restart the alarm-server(s)
